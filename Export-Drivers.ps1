#Requires -RunAsAdministrator
param($SavePath='.\drivers')

$SysInfo = Get-WmiObject Win32_ComputerSystem
$FolderName = "$($SysInfo.Manufacturer.Split(" ")[0]) $($SysInfo.Model)"

while($FolderName.Length -gt 32)
{
    # ISO labels can only be 32 chars
    $OverBy = $FolderName.Length - 32
    $FolderName = Read-Host -Prompt "The folder name '$FolderName' is too long by $OverBy characters, please enter a shorter name:"
}

$SavePath = Join-Path $SavePath $FolderName

if((Test-Path $SavePath) -eq $false)
{
    mkdir $SavePath -Force -ErrorAction SilentlyContinue | Out-Null
}

Write-Host "Exporting drivers for this system to '$SavePath'..."
Export-WindowsDriver -Destination $SavePath -Online
