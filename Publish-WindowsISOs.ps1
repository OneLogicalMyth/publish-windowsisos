#Requires -RunAsAdministrator

# Push location to save for later
Push-Location $PWD
Set-Location $PSScriptRoot

# read configuration and start logging
$Config = Get-Content .\config.json | ConvertFrom-Json
if((Test-Path $Config.logs_folder) -eq $false)
{
    Write-Host "Making the logs folder $($Config.logs_folder)"
    mkdir $Config.logs_folder -Force -ErrorAction SilentlyContinue
}
Start-Transcript -Path (Join-Path $Config.logs_folder 'creation_script.log') -Append

# add the dir2iso function needed to generate the ISOs
function DIR2ISO ($dir, $iso, $volume_name) {
 # https://gist.github.com/AveYo/c74dc774a8fb81a332b5d65613187b15
 if (!(test-path -Path $dir -pathtype Container)) {"[ERR] $dir\ :DIR2ISO";exit 1}; $dir2iso=@"
 using System; using System.IO; using System.Runtime.Interop`Services; using System.Runtime.Interop`Services.ComTypes;
 public class dir2iso {public int AveYo=2021; [Dll`Import("shlwapi",CharSet=CharSet.Unicode,PreserveSig=false)]
 internal static extern void SHCreateStreamOnFileEx(string f,uint m,uint d,bool b,IStream r,out IStream s);
 public static void Create(string file, ref object obj, int bs, int tb) { IStream dir=(IStream)obj, iso;
 try {SHCreateStreamOnFileEx(file,0x1001,0x80,true,null,out iso);} catch(Exception e) {Console.WriteLine(e.Message); return;}
 int d=tb>1024 ? 1024 : 1, pad=tb%d, block=bs*d, total=(tb-pad)/d, c=total>100 ? total/100 : total, i=1, MB=(bs/1024)*tb/1024;
 Console.Write("{0,3}%  {1}MB {2}  :DIR2ISO",0,MB,file); if (pad > 0) dir.CopyTo(iso, pad * block, Int`Ptr.Zero, Int`Ptr.Zero);
 while (total-- > 0) {dir.CopyTo(iso, block, Int`Ptr.Zero, Int`Ptr.Zero); if (total % c == 0) {Console.Write("\r{0,3}%",i++);}}
 iso.Commit(0); Console.WriteLine("\r{0,3}%  {1}MB {2}  :DIR2ISO", 100, MB, file); } }
"@; & { $cs=new-object CodeDom.Compiler.CompilerParameters; $cs.GenerateInMemory=1 #,# no`warnings
 $compile=(new-object Microsoft.CSharp.CSharpCodeProvider).CompileAssemblyFromSource($cs, $dir2iso) 
 $BOOT=@(); $bootable=0; $mbr_efi=@(0,0xEF); $images=@('boot\etfsboot.com','efi\microsoft\boot\efisys.bin') #,# efisys_noprompt
 0,1|% { $bootimage=join-path $dir -child $images[$_]; if (test-path -Path $bootimage -pathtype Leaf) {
 $bin=new-object -ComObject ADODB.Stream; $bin.Open(); $bin.Type=1; $bin.LoadFromFile($bootimage)
 $opt=new-object -ComObject IMAPI2FS.BootOptions; $opt.AssignBootImage($bin.psobject.BaseObject); $opt.Manufacturer='Microsoft' 
 $opt.PlatformId=$mbr_efi[$_]; $opt.Emulation=0; $bootable=1; $BOOT += $opt.psobject.BaseObject } }
 $fsi=new-object -ComObject IMAPI2FS.MsftFileSystemImage; $fsi.FileSystemsToCreate=4; $fsi.FreeMediaBlocks=0
 if ($bootable) {$fsi.BootImageOptionsArray=$BOOT}; $CONTENT=$fsi.Root; $CONTENT.AddTree($dir,$false); $fsi.VolumeName=$volume_name
 $obj=$fsi.CreateResultImage(); [dir2iso]::Create($iso,[ref]$obj.ImageStream,$obj.BlockSize,$obj.TotalBlocks) };[GC]::Collect()
}

if((Test-Path $Config.working_folder) -eq $false)
{
    Write-Host "Making the working folder $($Config.working_folder)"
    mkdir $Config.working_folder -Force -ErrorAction SilentlyContinue
}
if((Test-Path $Config.mount_folder) -eq $false)
{
    Write-Host "Making the mount folder $($Config.mount_folder)"
    mkdir $Config.mount_folder -Force -ErrorAction SilentlyContinue
}
$WorkingFolder = (Resolve-Path $Config.working_folder).Path
$MountPath = (Resolve-Path $Config.mount_folder).Path

Write-Host "Enumerating driver folders and building a list of ISOs to generate"
$DriverFolders = Get-ChildItem .\Drivers -Directory

Write-Host "Checking original Windows ISO..."
if((Test-Path $Config.windows_iso) -eq $false)
{
    throw "Windows ISO folder is missing, please update the configuration file with a valid path."
    exit 1
}
$OriginalImage = (Resolve-Path $Config.windows_iso).Path

Write-Host "Dismounting original Windows ISO if mounted already..."
Dismount-DiskImage -ImagePath $OriginalImage -StorageType ISO -ErrorAction SilentlyContinue

Write-Host "Mounting original Windows ISO..."
Mount-DiskImage -ImagePath $OriginalImage -StorageType ISO
$WinDir = (Get-PSDrive | ?{ $_.free -eq 0 }).Root

Write-Host "Emptying working folder ready for new work"
Remove-Item "$($WorkingFolder)\*" -Recurse -Force -ErrorAction SilentlyContinue

$DriverFolders | %{

    Write-Host "Copying source files from windows ISO..."    
    $SourceFiles = mkdir (Join-Path $WorkingFolder $_.BaseName) -Force -ErrorAction SilentlyContinue
    Copy-Item -Container "$WinDir*" -Destination $SourceFiles.FullName -Force -Recurse
    $WindowsImage = (Resolve-Path (Join-Path $SourceFiles.FullName 'sources\install.wim')).Path
    attrib.exe -r $WindowsImage # takes away readonly from the windows image

    Write-Host "Mounting Windows image $WindowsImage, and at path $MountPath"
    Mount-WindowsImage -Path $MountPath -ImagePath $WindowsImage -Name $config.windows_image_name -ErrorAction Stop -LogPath (Join-Path $config.logs_folder "mount_$($_.BaseName)_image.log")
    
    Write-Host "Adding windows drivers to image"
    Add-WindowsDriver -Path $MountPath -Driver $_.FullName -Recurse -LogPath (Join-Path $config.logs_folder "adding_$($_.BaseName)_drivers.log")

    Write-Host "Copying files to the ISO directory"
    foreach($image_file in $config.iso_files)
    {
        Write-Host "Copying file $($image_file.image)"
        Copy-Item $image_file.source (Join-Path $SourceFiles.FullName $image_file.image) -Force
    }

    Write-Host "Copying files to windows image"
    foreach($image_file in $config.image_files)
    {
        Write-Host "Copying file $($image_file.image)"
        mkdir (Join-Path $MountPath (Split-Path $image_file.image)) -Force -ErrorAction SilentlyContinue
        Copy-Item $image_file.source (Join-Path $MountPath $image_file.image) -Force
    }

    Write-Host "Loading default user registry hive"
    reg.exe load HKLM\TempUser (Join-Path $MountPath "Users\Default\NTUSER.DAT") | Out-Host

    Write-Host "Ammending default user registry"
    foreach($user_reg in $config.default_user_registry)
    {
        $base_cmd = "reg.exe $($user_reg.reg_action) HKLM\TempUser\$($user_reg.key)"
        if($user_reg.values -eq $null)
        {
            Write-Host "$($base_cmd) /f"
            Invoke-Expression "$($base_cmd) /f" | Out-Host
        }else{
            foreach($value in $user_reg.values)
            {
                $value_cmd = "$($base_cmd) /v `"$($value.property)`" /t $($value.type) /d `"$($value.value)`" /f"
                Write-Host $value_cmd
                Invoke-Expression $value_cmd | Out-Host
            }
        }
    }

    Write-Host "Unload default user registry hive"
    reg.exe unload HKLM\TempUser | Out-Host

    Write-Host "Loading SYSTEM registry hive"
    reg.exe load HKLM\TempSystem (Join-Path $MountPath "Windows\System32\config\SYSTEM") | Out-Host

    Write-Host "Ammending default user registry"
    foreach($hklm_reg in $config.hklm_SYSTEM_registry)
    {
        $base_cmd = "reg.exe $($hklm_reg.reg_action) HKLM\TempSystem\$($hklm_reg.key)"
        if($hklm_reg.values -eq $null)
        {
            Write-Host "$($base_cmd) /f"
            Invoke-Expression "$($base_cmd) /f" | Out-Host
        }else{
            foreach($value in $hklm_reg.values)
            {
                $value_cmd = "$($base_cmd) /v `"$($value.property)`" /t $($value.type) /d `"$($value.value)`" /f"
                Write-Host $value_cmd
                Invoke-Expression $value_cmd | Out-Host
            }
        }
    }

    Write-Host "Unload SYSTEM registry hive"
    reg.exe unload HKLM\TempSystem | Out-Host

    Write-Host "Remove AppxProvisioned Packages"
    Get-AppxProvisionedPackage -Path $MountPath | ?{ $_.DisplayName -in $config.remove_apps } | %{
        Write-Host "Removing AppxProvisioned Package $($_.DisplayName)"
        $_ | Remove-AppxProvisionedPackage -Path $MountPath
    }

    Write-Host "Dismounting windows image..."
    Dismount-WindowsImage -Path $MountPath -Save -LogPath (Join-Path $config.logs_folder "dismount_$($_.BaseName)_image.log")

    $ISOFile = ""
    if((Test-Path $config.iso_output_folder) -eq $false)
    {
        Write-Host "Making the ISO output folder $($Config.iso_output_folder)"
        mkdir $Config.iso_output_folder -Force -ErrorAction SilentlyContinue
    }

    $ISOFile = Join-Path (Resolve-Path $config.iso_output_folder).Path "$($_.BaseName).iso"
    Write-Host "Building Windows ISO to $ISOFile"
    DIR2ISO -dir $SourceFiles.FullName -iso $ISOFile -volume_name $_.BaseName | Out-Host    
}

# tidy up
Pop-Location
Dismount-DiskImage -ImagePath $OriginalImage -StorageType ISO
Stop-Transcript